# 修訂情報

| バージョン | 日付 | 修訂内容 |  |
| :---: | :-: | :--: | :- |
|  0.0  |    |      |  |
|       |    |      |  |
|       |    |      |  |
|       |    |      |  |

# はじめに

料理班は最終的には個々の研究成果を統合し、 1
つの料理ロボットとすることになるため、他人が扱えるよう環境やコーディングに配慮しなければならない。環境やプログラムをできるだけ班全体で共有することや、整理し文書化するといったことが重要である。

この資料は料理班のシステムの概要、環境構築手順と開発方法を紹介することを目的にし、まずは料理班のハードウェアとその周辺の説明、つぎは環境に必要なソフトウェアと環境構築手順、このあと、料理班に対して、最も重要な3つのハードウェアの注意事項を紹介し、最後は開発の方法と注意すべき点の説明と料理班共有環境の開発の説明。

できるだけ誰が読んでも理解できるように努めるが、完璧な文書というのは存在しない。疑問点や修正点等があれば、積極的に意見してほしい。

# システム概要

## 概略図

料理班のシステム大体の様子は

![全体的な画像](fig/Chapter1/SystemAll.png)

<span id="Fig_全体的な画像" label="Fig_全体的な画像">\[Fig\_全体的な画像\]</span>

今なら、PA10両手は同じものがついているので、この図は片手しか描かない。この図には「Your　PC」と「Share　PC」以外、全部2つある。各部分の詳細はこれから紹介する。

## PA10アーム

これは我々料理班の主役ーーPA10。これは三菱重工が開発した可搬式汎用の垂直多関節型ロボットアームで、重量は40kg、ACサーボモータにより\(±0.1mm\)の精度で動作できる。アームは，肩:3，肘:2，手先:2の計7自由度を持っており、手先には用途に合わせて力センサやロボットハンドなどを取り付けることができる。

この2つアームの区別方法はロボットの視点で左側のアームは「左アーム」という、右側のアームは「右アーム」という。だから、下の図の赤い枠線内部は左のPA10アーム。

![PA10アームの部分](fig/Chapter1/PA10.jpg)

<span id="Fig_PA10アームの部分" label="Fig_PA10アームの部分">\[Fig\_PA10アームの部分\]</span>

最後の関節は「ハンド」という。

## アタッチメント

アタッチメントはPA10のハンドに取り付けられたもので、今は3つのモジュールが付いている。

### 力覚センサー

Leptrino社製、型番：PFS080YA501U6S、6軸の力覚センサー(XYZの方向とXYZ軸周りのトルク)。主に力の制御するのに使う。下の図のオレンジ色の部分は力覚センサー。

![力覚センサー](fig/Chapter1/ForceSensor.jpg)

<span id="Fig_力覚センサー" label="Fig_力覚センサー">\[Fig\_力覚センサー\]</span>

力覚センサーの性能は下の図で示した。　

![力覚センサーの性能](fig/Chapter1/ForceSensor-performence.png)

<span id="Fig_力覚センサー仕様書" label="Fig_力覚センサー仕様書">\[Fig\_力覚センサー仕様書\]</span>

### 平行Gripper

力覚センサー下の真っ黒の箱とその下2つ平行銀色の板は「平行Gripper」という。省略は「グリーパ」。主に道具と物を掴むのに使う。

### RealSense D435

グリーパの前についたカメラは「RealSense
D435」というRGBDカメラ。このカメラはIntel製、今年1月発売開始したRealSenseシリーズの最新モデル。性能は以前使ったRealSense　F200より遥かに強い。RealSense
D435の特性は以下の通り。

1.  深度センサーの出力解像度：1280 x 720 @ 90fps

2.  深度センサーの視野角：水平85.2度，鉛直58度

3.  最大深度：約10m（キャリブレーション，環境光に依存）

4.  RGBカメラの出力解像度：1920 x 1080 @ 30fps

5.  RGBカメラの視野角：水平69.4度，鉛直42.5度

6.  カメラのサイズ：90mm x 25mm x 25mm

7.  カメラ側のコネクタ：USB 3.0 Type-C

8.  PC側のコネクタ：USB 3.0
Type-A

力覚センサーとグリーパに挟まれた「凹」のような板と下の「凸」の板はRealSenseを固定するものだ。

<span id="Fig_アタッチメント2" label="Fig_アタッチメント2">\[Fig\_アタッチメント2\]</span>

## テーブル

料理班がよく使うテーブルは3つがある。ロボットの前の「実験テーブル」と目の前の「左テーブル」と「右テーブル」。「実験テーブル」は名前とおり、実験を行いテーブル。「左テーブル」と「右テーブル」は実験する時パソコンの置き場。

<span id="Fig_料理班のテーブル" label="Fig_料理班のテーブル">\[Fig\_料理班のテーブル\]</span>

## PC

料理班が使っているPCが多い、以前なら4つのPCが使われていた。今回はGripperのRTCがUbuntuの共有PCに移行したので、今常に使っているPCは3台。

### PA10を制御するPC

これは各PA10に1台、計2台がある。左アームのPCはアームの左下にある、右アームのは2つアームの真ん中の下にある。

<span id="Fig_PA10を制御するPC" label="Fig_PA10を制御するPC">\[Fig\_PA10を制御するPC\]</span>

### Ubuntu共有PC

Ubuntu共有PCは「右テーブル」に設置しいるデスクトップ型のパソコン。普段よく使われているので、「共有PC」に省略する。このPCの目的はGripperのRTCを実行することと、我々のプログラムを実行すること。共有PCはデスクトップ型、サーバ用のPCで、性能はノートパソコンより遥かに強い、だから、自分のプログラムをこの共有PCに実行するのはおすすめだ。

### Windows共有PC

Windows共有PCは以前GripperのRTCを実行するPC。しかし、高級なパソコンはただ2つのRTCだけを実行するだけは無駄だと思って、RTCをUbuntu共有PCへ移行して、今Windows共有PCは暇になる。今後はWindowsでしかできないこと（例えば、CADソフトInventorで物を設計するなど）をする予定。

<span id="Fig_共有PC" label="Fig_共有PC">\[Fig\_共有PC\]</span>

## 電源について

料理班の設備が多いので、電源の接続は複雑だ。

### 電源ボックス

これはPA10専用の電源アダプター。左アームの電源は遠い昔、元の電源ボックスが壊れて、今井先輩が自作した電源モジュールだ。右アームは本来の姿。

<span id="Fig_電源ボックス" label="Fig_電源ボックス">\[Fig\_電源ボックス\]</span>

### 電源タップ

料理班の電源タップは左右のテーブルとも4個あり、PA10の後ろは3個ある。

#### 前のタップ

左右のテーブルの上の2つは自由に使うタップ。右のテーブルの後ろのタップは共有PC2つの電源、Ubuntu共有PCのモニターの電源、右のテーブルの左にあるルータの電源とモバイルディスクの電源。左のテーブルの後ろのタップはWindows共有PCのモニターの電源と前3つのタップの集め。

![前のタップの接続](fig/Chapter1/Tap-Front.jpg)

<span id="Fig_前のタップ" label="Fig_前のタップ">\[Fig\_前のタップ\]</span>

#### 後ろのタップ

後ろの設備が多いので、接続が少々複雑になる。

![後ろのタップの接続](fig/Chapter1/Tap-Back.jpg)

<span id="Fig_後ろのタップ" label="Fig_後ろのタップ">\[Fig\_後ろのタップ\]</span>

#### タップの接続

タップの接続は[\[Fig\_タップの接続\]](#Fig_タップの接続)の通り。

![タップの接続](fig/Chapter1/Tap-All.jpg)

<span id="Fig_タップの接続" label="Fig_タップの接続">\[Fig\_タップの接続\]</span>

### アタッチメントの電源

アダプターを使用するアタッチメントは「グリーパ」と「RealSense」だけ。「RealSense」の電源は[3.6.2.2](#Sec_後ろのタップ)の「Tap
6」で紹介した。「グリーパ」のアダプターは下の図に示した。

![グリーパのアダプター](fig/Chapter1/Adapter.jpg)

<span id="Fig_グリーパのアダプター" label="Fig_グリーパのアダプター">\[Fig\_グリーパのアダプター\]</span>

### UPSの周り

料理班はUPS二台使っている。位置はPA10右アームの電源と右アームのPCの間。

![UPS](fig/Chapter1/UPS.jpg)

<span id="Fig_UPS" label="Fig_UPS">\[Fig\_UPS\]</span>

## ネットの接続

料理班はルータ全部二台を使う。一つ目はPA10を固定するフレームにある「BUFFALO」社製ルータで、二番目は右テーブルの左側面につけた白い小さなルータ。

![ルータ](fig/Chapter1/Route.jpg)

<span id="Fig_Route" label="Fig_Route">\[Fig\_Route\]</span>

上の図で、オレンジ色のケーブルは研究室のネットワークに接続しているケーブルで、どっちが抜けたら、プログラムが実行不能になる。

## RTCについて

### 料理班のRTC

料理班が使っているRTCは6つがある。「PA10」、「Gripper」、「RealSenseD435」、「RealSenseF200」、「ImageProcess」と「PointCloudProcess」。各RTCを実行するPCは下の図で示す。

![RTCを実行するPC](fig/Chapter1/RTC-Comp.jpg)

<span id="Fig_RTC-Comp" label="Fig_RTC-Comp">\[Fig\_RTC-Comp\]</span>

### RealSenseのRTC

料理班は今年RealSenseF200からRealSenseD435に移行したが、先輩たちのプログラムは古いRealSenseで書いた。もし、誰か先輩のプログラムを動かしたいなら、古いRealSenseのRTCが必要と思われる。そこで、今年は、2つバージョンのRealSenseのRTC全部提供する。誰も使わないなら、来年古いRealSenseのRTCを消す。

古いRealSenseのRTCの名前は「RTC\_Realsense」で、以前のRTCを区別するため「RTC\_RealsenseD435」にした。そして、今回RealSenseD435がPA10の両手に取り付けて、以前の「ImageProcess」と「PointCloudProcess」のRTCを同時四台RealSenseまで接続できるように編集した。

![RTC\_RealSenseの周りの接続](fig/Chapter1/RTC-RS.jpg)

<span id="Fig_RTC-RS" label="Fig_RTC-RS">\[Fig\_RTC-RS\]</span>

### 全体的な接続図

この図は各RTCと各Pythonのクラスと自分のメインスクリプトとの関係。

![全体的な接続図](fig/Chapter1/RTC-All.png)

<span id="Fig_RTC-All" label="Fig_RTC-All">\[Fig\_RTC-All\]</span>

# 環境構築

## 開発に使われるソフト

eeee

### TimeShift

### GitKraken

### QT Creator

### PyCharm

## 便利になるTips

### 入力補完の大文字小文字の区別を取り去る

タブで入力コマンドの補完をする際、大文字と小文字の区別なしで補完してほしい場合が多々ある。
こういうときのために、入力補完の大文字小文字の区別を取り去っておく。
以下のコードを実行して、ターミナルを再起動をすれば、完成だ。

    sudo sh -c "echo \"set completion-ignore-case on\">>/etc/inputrc"

### ショットカットを使いましょう！

#### スクリーンショット

### PCDファイルをダブルクリックで開く

### pycファイル作成禁止

## 依頼ライブラリの導入

### librealsense for RealSense D435

### librealsense for RealSense F200

### VTK

### PCL

### OpenCV

### Pythonの周り

### Open3D(Optional)

## 開発環境の構築

<div id="MakeDevEnv">

</div>

## 構築のあと

### 構築の検証

### 開発の準備

#### RemminaでPA10PCの遠隔操作

ddddddd

# 開発する前の注意事項

## PA10

## Gripper

## RealSense D435

### 内部パラメータのキャリブレーション

# 開発

eee

## 料理班のPCの使い方

### PA10のPC

### 共有PC

## 各モジュールの動かす方法

### EnvSetup.pyの使い方

### PA10

#### f21pa10class

#### 動作の種類

#### move\_rmrc()

### Gripper

### RealSense D435

### HandとCameraのキャリブレーション

## RTCの開発について

### RTC\_RealsenseD435

共有環境のRTC

### RTC\_parrallelGripper

共有環境のRTC

### RTC\_Realsense\_Old

共有環境のRTC

### RTC\_ImageProcess

「base」のRTC

### RTC\_PointCloudProcess

「base」のRTC

## GITの使い方と共有環境の開発

Gitの使い方は最初自分が書きたいが、Gitのオフィシャルホームページにという本がある。この本を読んで、Gitの使い方を身に付けよう！

全部読む必要はない。1.1、1.3、1.7、2の全部、3の全部、5.2-統合マネージャー型のワークフロー、7.11を読めば、十分だ。

### Fork\&Pull Request Work Flow

料理班の人数が多くて、料理班の共有環境やキャリブレーションデータの更新がとても大変だと思われる。そこで、「Fork
Workflow」という流れを使う。「Fork
Workflow」はOpenCV、librealsense、pclなどのライブラリーの開発に使われている仕事の流れ。

## フォルダーの構成

eee

### 「base」フォルダー

#### 「data」フォルダー

#### 「rtc」フォルダー

#### 「scripts」フォルダー

#### 「tools」フォルダー

### 「share」フォルダー

#### 「data」フォルダー

#### 「rtc」フォルダー

#### 「scripts」フォルダー

#### 「tools」フォルダー

## 研究報告

### PythonのプログラムをCtrl-Cで閉じられるようになる

以前のプログラムはRTCが使ったら、プログラムが終わっても、正常に閉じられない。

### 料理班開発の流れ

料理班の人数が多くて、料理班の共有環境やキャリブレーションデータの更新がとても大変だと思われる。そこで、「Fork
Workflow」という流れを使う。「Fork
Workflow」はOpenCV、librealsense、pclなどのライブラリーの開発に使われている仕事の流れ。

### OpenRTPのインストールについて

前のopenrtpがインストール不能の原因が解明した。Linuxで「git
clone」でクローンしたリポジトリの大きさが30MBぐらいだけ。openrtpの大きさは130Byte。しかし、Windowsで正常にダウンロードしてた。したがって、下の「wget」で、直接にopenrtpをダウンロードすれば、正常にインストールできる。

``` 
git clone https://github.com/n-ando/xenial_package.git

cd xenial_package/xenial/main/binary-amd64/

sudo dpkg -i openrtm-aist_1.1.2-0_amd64.deb
sudo dpkg -i openrtm-aist-example_1.1.2-0_amd64.deb
sudo dpkg -i openrtm-aist-dev_1.1.2-0_amd64.deb

sudo dpkg -i openrtm-aist-python_1.1.2-1_amd64.deb
sudo dpkg -i openrtm-aist-python-example_1.1.2-1_amd64.deb

wget https://media.githubusercontent.com/media/n-ando/xenial_package/master/xenial/main/binary-amd64/openrtp_1.2.0-0_amd64.deb
sudo dpkg -i openrtp_1.2.0-0_amd64.deb
    
```

<span>3</span> 料理班環境資料 ver0.99, 長野 恵典
