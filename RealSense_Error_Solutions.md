RealSense_Error_Solutions
=========================

### 万能な方法
もちろん、再起動だ。

### VIDIOC_DQBUF error 19 , No such device
問題は下の図のように：  
![Realsense_NoUsbHUB_ERROR](fig/Realsense_NoUsbHUB_ERROR.png)  
**解決方法**：USB3.0HUBをつかって、RealSenseを挿せば解決できる。

### Cannot access /sys/class/video4linux
**解決方法**：下のコードを実行してください。
```bash
cd ~/Documents/librealsense
./scripts/patch-realsense-ubuntu-xenial.sh
```
