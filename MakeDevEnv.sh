#!/bin/bash

set -x
shellFileDir=`cd \`dirname $0\`; pwd`
######################################################################################################################
#############################################################
# 1. Start to setup repositories and development environment.
#############################################################
######################################################################################################################
#if false;then
#####################################
# 1-1. Input Git account information.
#####################################
read -p "Please input your username of Lab's Git:" Username
read -s -p "Please input your password:" Password

##########################################
# 1-2. Get cookies and login to Lab's git.
##########################################
curl -c tmpcookies 'http://192.168.0.9:8080/'
curl -b tmpcookies -c tmpcookies 'http://192.168.0.9:8080/signin' --data "userName=${Username}&password=${Password}" --compressed

###################################
# 1-3. Send fork request to server.
###################################
curl -b tmpcookies -c tmpcookies 'http://192.168.0.9:8080/New_Cook_Team/share/fork' --data "account=${Username}" --compressed
curl -b tmpcookies -c tmpcookies 'http://192.168.0.9:8080/New_Cook_Team/HandCalibrationData/fork' --data "account=${Username}" --compressed
curl -b tmpcookies -c tmpcookies 'http://192.168.0.9:8080/New_Cook_Team/PythonEnvSetup/fork' --data "account=${Username}" --compressed
curl -b tmpcookies -c tmpcookies 'http://192.168.0.9:8080/New_Cook_Team/RealsenseUtil/fork' --data "account=${Username}" --compressed
curl -b tmpcookies -c tmpcookies 'http://192.168.0.9:8080/New_Cook_Team/RTC_RealsenseD435/fork' --data "account=${Username}" --compressed
curl -b tmpcookies -c tmpcookies 'http://192.168.0.9:8080/New_Cook_Team/RTC_Realsense_Old/fork' --data "account=${Username}" --compressed
curl -b tmpcookies -c tmpcookies 'http://192.168.0.9:8080/New_Cook_Team/base/fork' --data "account=${Username}" --compressed
curl -b tmpcookies -c tmpcookies 'http://192.168.0.9:8080/New_Cook_Team/RTC_PointCloudProcess/fork' --data "account=${Username}" --compressed
curl -b tmpcookies -c tmpcookies 'http://192.168.0.9:8080/New_Cook_Team/RTC_ImageProcess/fork' --data "account=${Username}" --compressed
curl -b tmpcookies -c tmpcookies 'http://192.168.0.9:8080/New_Cook_Team/HandCalibration/fork' --data "account=${Username}" --compressed

################################################
# 1-4. Delete cookies and logout from Lab's git.
################################################
curl -b tmpcookies -c tmpcookies 'http://192.168.0.9:8080/signout' --compressed
rm ./tmpcookies


######################################################################################################################
#############################################################
# 2. Start to setup repositories and development environment.
#############################################################
######################################################################################################################
cd ~
mkdir  Workspace
cd Workspace

PC_UserName=`whoami`

###########################################
# 2-1. Clone the share and base Repository.
###########################################
git clone http://192.168.0.9:8080/git/${Username}/share.git
git clone http://192.168.0.9:8080/git/${Username}/base.git ${PC_UserName}

###########################################################################
# 2-2. Set .gitmodules file. Make sub modules' upstream to your own stream.
###########################################################################
cd ~/Workspace/${PC_UserName}
git rm -r ./rtc/RTC_ImageProcess
git rm -r ./rtc/RTC_PointCloudProcess
git rm -r ./scripts/Calibration
git submodule add -f http://192.168.0.9:8080/git/${Username}/RTC_ImageProcess.git ./rtc/RTC_ImageProcess
git submodule add -f http://192.168.0.9:8080/git/${Username}/RTC_PointCloudProcess.git ./rtc/RTC_PointCloudProcess
git submodule add -f http://192.168.0.9:8080/git/${Username}/HandCalibration.git ./scripts/Calibration

cd ~/Workspace/share
git rm -r ./data/handCamCalib
git rm -r ./tools/EnvSetup
git rm -r ./rtc/RTC_RealsenseD435
git rm -r ./rtc/RealsenseUtil
git rm -r ./rtc/RTC_Realsense_Old
git submodule add -f http://192.168.0.9:8080/git/${Username}/HandCalibrationData.git ./data/handCamCalib
git submodule add -f http://192.168.0.9:8080/git/${Username}/PythonEnvSetup.git ./tools/EnvSetup
git submodule add -f http://192.168.0.9:8080/git/${Username}/RTC_RealsenseD435.git ./rtc/RTC_RealsenseD435
git submodule add -f http://192.168.0.9:8080/git/${Username}/RealsenseUtil.git ./rtc/RealsenseUtil
git submodule add -f http://192.168.0.9:8080/git/${Username}/RTC_Realsense_Old.git ./rtc/RTC_Realsense_Old

##########################################################################
# 2-3. Add remote stream and create Push_CookTeam branch at master branch.
##########################################################################
cd ~/Workspace/${PC_UserName}/
git remote rename origin Mine
git remote add Cook_Team http://192.168.0.9:8080/git/New_Cook_Team/base.git
git branch Push_CookTeam Mine/Push_CookTeam

cd ~/Workspace/${PC_UserName}/rtc/RTC_ImageProcess
git remote rename origin Mine
git remote add Cook_Team http://192.168.0.9:8080/git/New_Cook_Team/RTC_ImageProcess.git
git branch Push_CookTeam Mine/Push_CookTeam

cd ~/Workspace/${PC_UserName}/rtc/RTC_PointCloudProcess
git remote rename origin Mine
git remote add Cook_Team http://192.168.0.9:8080/git/New_Cook_Team/RTC_PointCloudProcess.git
git branch Push_CookTeam Mine/Push_CookTeam

cd ~/Workspace/${PC_UserName}/scripts/Calibration
git remote rename origin Mine
git remote add Cook_Team http://192.168.0.9:8080/git/New_Cook_Team/HandCalibration.git
git branch Push_CookTeam Mine/Push_CookTeam


cd ~/Workspace/share
git remote rename origin Mine
git remote add Cook_Team http://192.168.0.9:8080/git/New_Cook_Team/share.git

cd ~/Workspace/share/rtc/RealsenseUtil
git remote rename origin Mine
git remote add Cook_Team http://192.168.0.9:8080/git/New_Cook_Team/RealsenseUtil.git

cd ~/Workspace/share/rtc/RTC_RealsenseD435
git remote rename origin Mine
git remote add Cook_Team http://192.168.0.9:8080/git/New_Cook_Team/RTC_RealsenseD435.git

cd ~/Workspace/share/rtc/RTC_Realsense_Old
git remote rename origin Mine
git remote add Cook_Team http://192.168.0.9:8080/git/New_Cook_Team/RTC_Realsense_Old.git

cd ~/Workspace/share/tools/EnvSetup
git remote rename origin Mine
git remote add Cook_Team http://192.168.0.9:8080/git/New_Cook_Team/PythonEnvSetup.git

cd ~/Workspace/share/data/handCamCalib
git remote rename origin Mine
git remote add Cook_Team http://192.168.0.9:8080/git/New_Cook_Team/HandCalibrationData.git

#fi
#PC_UserName=`whoami`
#cd ~/Workspace/share/data/handCamCalib
##########################################################################
# 2-4. Add remote stream and create Push_CookTeam branch at master branch.
##########################################################################
cd $shellFileDir
cp -rf './GitIgnore Files/RTC_ImageProcess/1.gitignore' ~/Workspace/${PC_UserName}/rtc/RTC_ImageProcess/.gitignore
cp -rf './GitIgnore Files/RTC_PointCloudProcess/1.gitignore' ~/Workspace/${PC_UserName}/rtc/RTC_PointCloudProcess/.gitignore
cp -rf './GitIgnore Files/RTC_RealsenseD435/1.gitignore' ~/Workspace/share/rtc/RTC_RealsenseD435/.gitignore
cp -rf './GitIgnore Files/RTC_Realsense_Old/1.gitignore' ~/Workspace/share/rtc/RTC_Realsense_Old/.gitignore
cp -rf './GitIgnore Files/RealsenseUtil/1.gitignore' ~/Workspace/share/rtc/RealsenseUtil/.gitignore
cp -rf './GitIgnore Files/base/1.gitignore' ~/Workspace/${PC_UserName}/.gitignore
cp -rf './GitIgnore Files/share/1.gitignore' ~/Workspace/share/.gitignore

#exit 0
##########################################################################
# 2-5. Compile.
##########################################################################
cd ~/Workspace/share/rtc/RTC_RealsenseD435
cmake .
(make -j16 || make -j16 || make -j16 || sleep 30)>>~/makeOut.txt
make
sleep 7

cd ~/Workspace/share/rtc/RTC_Realsense_Old
cmake .
(make -j16 || make -j16 || make -j16 || sleep 30)>>~/makeOut.txt
make
sleep 7

cd ~/Workspace/${PC_UserName}/rtc/RTC_PointCloudProcess
cmake .
(make -j16 || make -j16 || make -j16 || sleep 30)>>~/makeOut.txt
make
sleep 7

cd ~/Workspace/${PC_UserName}/rtc/RTC_ImageProcess
cmake .
(make -j16 || make -j16 || make -j16 || sleep 30)>>~/makeOut.txt
make
sleep 7

grep -n "PYTHONDONTWRITEBYTECODE=True" /etc/profile || \
sudo sh -c "\
echo \"PYTHONDONTWRITEBYTECODE=True\n\
export PYTHONDONTWRITEBYTECODE\" >> /etc/profile"

sudo sh -c "echo \"PYTHONDONTWRITEBYTECODE=True\" >> /etc/profile"
sudo sh -c "echo \"export PYTHONDONTWRITEBYTECODE\" >> /etc/profile"

if[-e /usr/local/lib/python2.7/dist-packages/WorkspaceDirInfo.py &&
    -e /usr/local/lib/python2.7/dist-packages/EnvSetupVar.py &&] then
    -e /usr/local/lib/python2.7/dist-packages/EnvSetup.py &&
else
    echo "Your have make soft link with WorkspaceDirInfo.py EnvSetup.py and EnvSetupVar.py"
fi
    bash ./MakeLink.sh
