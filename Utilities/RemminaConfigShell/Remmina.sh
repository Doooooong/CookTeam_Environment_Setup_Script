#!/bin/bash

timestr=`date +%s%N`
cp ./left.remmina ~/.remmina/${timestr:0:13}.remmina

sleep `echo "scale=5;${RANDOM}/100000+0.2" | bc -l`

timestr=`date +%s%N`
cp ./right.remmina ~/.remmina/${timestr:0:13}.remmina
