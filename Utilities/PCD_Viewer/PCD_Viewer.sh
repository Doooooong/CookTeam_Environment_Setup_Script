#!/bin/bash

sudo cp ./PCD_Viewer.desktop /usr/share/applications/
sudo cp ./pcl_logo.png /usr/share/icons/

grep -n "image/x-photo-cd=PCD_Viewer.desktop" /etc/gnome/defaults.list || \
sudo bash -c "echo \"image/x-photo-cd=PCD_Viewer.desktop\" >> /etc/gnome/defaults.list"
