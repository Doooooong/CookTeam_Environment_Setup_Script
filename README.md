SUEHIRO\_KUDOU\_LAB
===================
CookTeam\_Environment\_Setup
============================

LANGUAGE
-----------
- [日本語](#目次)
- [中文（简体）](#README_CHS.md)
- English????

日本語
===========

目次
-----------
* [構築前](#構築前)
* [構築開始](#構築開始)
  * [カーネルバージョンの確認](#カーネルバージョンの確認)
  * [入力補完の大文字小文字の区別を取り去る](#入力補完の大文字小文字の区別を取り去る)
  * [librealsense2](#librealsense2のコンパイルとインストール)
  * [librealsense-old](#librealsense-oldのコンパイルとインストール)
  * [VTK](#vtkインストール)
  * [PCL](#pclライブラリのコンパイルとインストール)
  * [OpenCV](#opencvのコンパイル)
  * [OpenRTM](#openrtmのインストール)
  * [Pythonのまわり](#pythonのまわり)
  * [パスの構築](#開発環境のパスの設定)
    - [開発用フォルダーの設定](#開発用のフォルダーの設定)
    - [パス設定](#パスの設定)
  * [RTCコンパイル](#rtcのコンパイル)
* [インストール検証](#インストール検証)

構築前
-----------
### 必要なソフトウェア
1. GitKraken  
LinuxとWindows両方使えるGitのGUI。「コンフリクト処理」と「差分処理」は全部このソフトでできる。そして、ブランチ、コミット、マージの間の関係ははっきり。  

  ![fig/GUI](fig/gui.png)
  ![fig/Branch](fig/branch.png)  
  
  インストール手順は：
```shell
cd ~/Documents/
wget https://release.gitkraken.com/linux/gitkraken-amd64.deb
sudo dpkg -i gitkraken-amd64.deb
```
起動と使い方は「」に参照してください。
2. TimeShift  
Linuxのバックアップソフトで、システムが起動入れる限り、あなたのPCを回復できる。このソフトはGitのようにあなたのPCをバックアップする。

  ![fig/TimeShift_GUI](fig/timeshift_gui.png)

  インストール手順は：
```shell
sudo apt-add-repository -y ppa:teejee2008/ppa
sudo apt-get update
sudo apt-get install timeshift
```
起動と使い方は「」に参照してください。

### おすすめのソフトウェア
1. C++開発：QT Creator  
　このソフトはQTアプリを開発することを目的にするが、C++のプログラムの編集もできる。自分がLinuxでいろいろなC++のエディター、IDE（gedit,eclips,code lite...）を使ったことがあった。  
　geditはただのエディターで、一つのC++ファイルは大丈夫が、RTCはプロジェクト本体だけ何十個の.cpp.hppファイルで、依頼ライブラリも加えば、数え切れないほどの量。だから、関数やクラスや変数などの定義が見たい時、geditは無理。  
　Eclipsは優秀なIDEが、うちのRTCのプロジェクトを導入して、どう設定しても、クラスや関数の定義が見つからなくなって、諦めました。もし、先の問題が解決できれば、Eclipsもおすすめです。  
　最後は主役の「QT Creator」。このソフトにRTCのプロジェクトをインポートして、includeのディレクトリを設定すれば、関数やクラスや変数の定義が「F2」キーで簡単に見える。もっと設定すれば、このソフトでコンパイルもできる。  
インストール手順は：
```shell
cd ~/Documents/
wget http://ftp.yz.yamagata-u.ac.jp/pub/qtproject/archive/online_installers/3.0/qt-unified-linux-x64-3.0.4-online.run
chmod +x ./qt-unified-linux-x64-3.0.4-online.run
./qt-unified-linux-x64-3.0.4-online.run
```
開いたウィンドウで操作すればインストールできる。
2. Python開発：PyCharm  
　おそらくこのソフトはPythonの最強のIDEだと思う。文法チェック、文法ハイライト、自動フォーマット、コードの実行、デバッグなど。ほぼ、pythonの開発に全部必要なものがこのソフトに含まれだ。PythonのIDEはこれしかない！！！！

  ![fig/pycharm_gui](fig/pycharm_gui.png)

  インストール手順は：
```shell
cd ~/Documents/
wget https://download.jetbrains.com/python/pycharm-community-2018.1.2.tar.gz
tar -xzvf pycharm-community-2018.1.2.tar.gz
sudo mkdir /usr/share/bin
mv ./pycharm-community-2018.1.2 /usr/share/bin/pycharm
sudo chown `whoami` /usr/share/bin/pycharm
cd /usr/share/bin/pycharm/bin
chmod +x ./pycharm.sh
./pycharm.sh
```
このコードで、PyCharmが起動する。そして、Lancherに固定すれば、いつでも実行できる。

Bashファイルを使いたいなら
、[Bashファイル説明](BashFile_README.md)を参照してください。
新人なら、この手順を読みながら、環境を構築するのはおすすめ。

構築開始
-----------
### カーネルバージョンの確認
　`uname -r`をターミナルに入力し、自分のシステムのカーネルバージョンを確認する。バージョンは`4.4.XX`、`4.8.XX`、`4.10.XX`なら、大丈夫です。次のセクションを入ってください（入る前に、下の`Note`を見てください。）。もし`4.13.XX`なら、おめでとう、まずは、カーネルバージョンを`4.10.XX`にしてください。[カーネルバージョンの切り替え手順](ChangeKernal.md)
>**Note:**しかし、4.4.XX,4.8.XX,4.10.XXの人は安心してはだめ。4.13.XXのカーネルはないなら（`dpkg -l | grep linux-image`この命令で確認する。）、ソフトウェアをアップデートする命令`sudo apt-get dist-upgrade`を実行すれば、自動的に4.13.XXのカーネルにアップデートする不安要素がある（要検証）。したがって、[カーネルバージョンの切り替え手順](ChangeKernal.md)でカーネルを正しいバージョンにした後、環境構築をするのはお勧め。ちなみに、カーネルバージョンの問題はRealSenseに影響する。したがって、RTC_Realsenseが立ち上げられなくなって、librealsenseのexampleも実行不能になったら、まずはカーネルのバージョンを確認してください。

### 入力補完の大文字小文字の区別を取り去る
Linuxのターミナルで、命令を入力する時、前の二三文字だけを入力し、`Tab`キーをと、Linuxが残した部分を自動的に補完する。普通の場合は大文字小文字の区別がある。ないのほうが便利。
```shell
sudo sh -c "echo \"set completion-ignore-case on\">>/etc/inputrc"
```

### librealsense2のコンパイルとインストール
まず、システムのソフトウェアをアップデートする。
```shell
sudo apt-get update
sudo apt-get update --fix-missing
sudo apt-get upgrade
```
git命令でlibrealsenseのソースコードを獲得する。
```shell
cd ~/Documents/
git clone https://github.com/IntelRealsense/librealsense.git
```
そして、librealsenseの依頼ライブラリをインストールする。（`-y`オプションは`apt-get install`で、`continue install?[Y/n]`の時`y`を自動で選択する。）
```shell
sudo apt-get -y install libusb-1.0-0-dev pkg-config
sudo apt-get -y install libglfw3-dev libgtk-3-dev
```
次はコンパイルとインストールのセクション。
まずは`librealsense`で`build`フォルダーを作り，入る。
```bsah
cd ~/Documents/librealsense
mkdir build
cd build
```
そして、cmakeでmakefileを作成し，コンパイルとインストール一気に完成する。（`make -j4`の`-j4`はマルチスレッドコンパイルで、`4`は使ているコアの数、研究室のパソコンは四コア。）
```shell
cmake .. -DBUILD_EXAMPLES=true -DBUILD_PYTHON_BINDINGS=bool:true -DPYTHON_EXECUTABLE=/usr/bin/python2.7
make -j4
sudo make install
sudo mkdir /usr/lib/python2.7/dist-packages/
sudo cp ./wrappers/python/pyrealsense2.so /usr/local/lib/python2.7/dist-packages/
```
RealSenseが使いたいなら、カーネルにパッチを入れる必要がある。
**WARNING:**RealSenseを**[-挿さらない-]**で、下のコードを実行してください：
```shell
cd ~/Documents/librealsense
sudo cp config/99-realsense-libusb.rules /etc/udev/rules.d/
sudo udevadm control --reload-rules && udevadm trigger
sudo apt-get -y install libssl-dev
./scripts/patch-realsense-ubuntu-*.sh
```

### librealsense-oldのコンパイルとインストール
さっきと同じように、ソースコードをクローンする。
```shell
cd ~/Documents/
git clone https://github.com/IntelRealsense/librealsense.git librealsense-old
```
RealSense F200が古すぎで、最新版のlibrealsenseが対応していないから、以前のバージョンを切り替える。
```shell
cd ~/Documents/librealsense-old #librealsenseのディレクトリに切り替える
git branch -a #リモートブランチを見る
git checkout -b legacy origin/legacy #ブランチを切り替える
```
次はコンパイルとインストールのセクション。
まずは`librealsense`で`build`フォルダーを作り，入る。
```shell
cd ~/Documents/librealsense-old
mkdir build
cd build
```
```shell
cmake .. -DBUILD_EXAMPLES=true
make -j4
sudo make install
```

```shell
cd ~/Documents/librealsense
cp `dirname $0`/Realsense/*.* ~/Documents/librealsense-old/scripts
./scripts/patch-realsense-ubuntu-*.sh
```
>**コード説明**：
- **自分で命令を入力する人**なら、四行目の`` `dirname $0` ``（注意！これは**バッククォート**。`Shift+@`で入力する（JISのキーボード）。英語のキーボードは`1`の左のキーだ）を`このbashファイルのフォルダー`に入れ替える。例えば：
```shell
cp ~/Documents/CookTeam_Environment_Setup_Script/Realsense/*.* ~/Documents/librealsense-old/scripts
```

最後、librealsenseライブラリのパスを環境変数`LD_LIBRARY_PATH`に増加する。
```shell
sudo sh -c "echo \"export LD_LIBRARY_PATH=\\\${LD_LIBRARY_PATH}:/usr/local/lib/:/usr/local/lib/x86_64-linux-gnu/:\" >> /etc/profile"
source /etc/profile
sudo ldconfig
```
>**コード説明**：
- 一行目は`/etc/profile`に環境変数を設置する文を増加する文。Linuxで、自分で設置した環境変数はシャットダウンで消してしまう。`/etc/profile`このファイルはシステムの起動する時ですべでのユーザーに実行する。つまり、自分で設置した環境変数を永遠にシステムに増加した。
- 二行目は今のターミナルで`/etc/profile`を実行する。
- 三行目は今検索できるライブラリのキャッシュデータを作る命令。具体的には[ldconfig命令](http://man.linuxde.net/ldconfig "打不开？自己百度呀！")。

終わったら、`export | grep LD_LIBRARY_PATH`で環境変数`LD_LIBRARY_PATH`があるかどうかを確認する。存在すれば、下の図と同じはず。
![Env_Var_librealsense.png](fig/Env_Var_librealsense.png)
>**Note：**
- `source`文は今のターミナルで有効するから、**bashファイルで構築する人**はほかのターミナルでこの環境変数が見えない。
- 実は今この環境変数がなくても環境構築に影響はない。インストールの検証する前に再起動すれば大丈夫です。

### VTKインストール
VTKライブラリはデータを可視化するライブラリ。料理班にとって、後構築するOpenCVとPCLこの二つライブラリの中でVTKが使われた。したがって、VTKがうまくインストールされないなら、いろいろなわけわからないエラーが出るよ～～。
VTKが重要だが，構築する手順はとても簡単。
```shell
sudo apt-get update
sudo apt-get update --fix-missing
sudo apt-get upgrade
sudo apt-get install libxt-dev
cd ~/Documents
wget https://www.vtk.org/files/release/8.1/VTK-8.1.0.tar.gz
wget https://www.vtk.org/files/release/8.1/VTKData-8.1.0.tar.gz
tar -xzvf VTK-8.1.0.tar.gz
tar -xzvf VTKData-8.1.0.tar.gz
cd ~/Documents/VTK-8.1.0
mkdir build
cd build
cmake ..
make -j4
sudo make install
```
エラーがないなら、VTKの構築は終わり。**TODO:VTKの安定版を探す**。
### PCLライブラリのコンパイルとインストール
PCLのフルネームはPoint Cloud Library。つまり、これは点群を処理するライブラリ。構築にはVTKと同じだが、PCLの依頼ライブラリはVTKより遥かに多い。もしbashファイルを見たら、PCLのセクションの中にすごく長い部分がある、その部分は依頼ライブラリのインストール。もう一つの違いは、PCLはとても大きくで、マルチスレッドコンパイル使っても、45min以上かかる。したがって、料理班の環境構築で、この段階だけでパソコンを見なくでも大丈夫。
```shell
sudo apt-get update
sudo apt-get update --fix-missing
sudo apt-get upgrade
cd ~/Documents
git clone https://github.com/PointCloudLibrary/pcl.git pcl
sudo apt-get -y install cmake cmake-gui doxygen libphonon-dev mpi-default-dev openmpi-bin openmpi-common libflann1.8 libflann-dev libeigen3-dev libboost-all-dev libqhull* libusb-dev libgtest-dev git-core freeglut3-dev pkg-config build-essential libxmu-dev libxi-dev libusb-1.0-0-dev graphviz mono-complete qt-sdk openjdk-8-jdk openjdk-8-jre phonon-backend-gstreamer phonon-backend-vlc
cd ~/Documents/pcl
mkdir build
cd build
cmake ..
make -j4
sudo make install
```
VTKと同じで、エラーがないなら、VTKの構築は終わり。
### OpenCVのコンパイル
有名なOpenCVは紹介する必要がないよね。。。  
新人セミナーのときがインストルしたが、料理版はRealSenseが使うため、OpenCVのソースコードを編集しなければならない。だから、再コンパイル必要がある。
```shell
sudo apt-get update
sudo apt-get update --fix-missing
sudo apt-get upgrade
cd ~/Documents
git clone https://github.com/opencv/opencv.git OpenCV-master
sudo apt-get -y install build-essential libgtk2.0-dev libjpeg-dev libtiff5-dev libjasper-dev libopenexr-dev cmake python-dev python-numpy python-tk libtbb-dev libeigen3-dev yasm libfaac-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev libx264-dev libqt4-dev libqt4-opengl-dev sphinx-common texlive-latex-extra libv4l-dev libdc1394-22-dev libavcodec-dev libavformat-dev libswscale-dev default-jdk ant
```

```shell
cd ~/Documents/OpenCV-master
sed -i "s/\/\/#define OPENCV_TRAITS_ENABLE_DEPRECATED/#define OPENCV_TRAITS_ENABLE_DEPRECATED/" ~/Documents/OpenCV-master/modules/core/include/opencv2/core/traits.hpp
mkdir build
cd build
cmake -D VTK_DIR=/usr/local/lib/cmake/vtk-8.1\
		-D CMAKE_CXX_FLAGS=-std=c++11\
		-D CMAKE_BUILD_TYPE=RELEASE\
		-D CMAKE_INSTALL_PREFIX=/usr/local\
		-D WITH_TBB=ON\
		-D BUILD_NEW_PYTHON_SUPPORT=ON\
		-D WITH_V4L=ON\
		-D WITH_FFMPEG=OFF\
		-D BUILD_opencv_python2=ON\
		..
make -j4
sudo make install
```

インストールした後、PythonにOpenCVをインストールする。
```shell
sudo sh -c 'echo "/usr/local/lib">>/etc/ld.so.conf.d/opencv.conf'
sudo ldconfig
sudo mkdir /usr/local/lib/python2.7/site-packages/
sudo mkdir /usr/local/lib/python2.7/dist-packages/
sudo cp ~/Documents/OpenCV-master/build/lib/cv2.so /usr/lib/python2.7/site-packages/
sudo cp ~/Documents/OpenCV-master/build/lib/cv2.so /usr/lib/python2.7/dist-packages/
```
>**コード説明**：
- 最後の四行はPythonにOpenCVをインストールする。

### OpenRTMのインストール
特別な事情がないと、下の「普段のインストール方法」でインストールしてください。
#### 普段のインストール方法
OpenRTMの紹介は要る？
```shell
mkdir ~/Documents/openRTM-aist_install/
cd ~/Documents/openRTM-aist_install/
wget http://svn.openrtm.org/OpenRTM-aist/trunk/OpenRTM-aist/build/pkg_install_ubuntu.sh
sudo sh ./pkg_install_ubuntu.sh -l c++ -l python -l openrtp -d --yes
```
>**コード説明**：
- 三行目：ネットでインストールファイルをダウンロードする。
- 最後の行：OpenRTMをインストールする。後ろのオプションはオフィシャルの手引きで加えたもの。（c++版、python版とopenrtpをインストールする。具体的なオプション説明は[オプション説明](https://openrtm.org/openrtm/ja/node/6345)）で参照してください。

実は2017年のある更新で、OpenRTMのインストールが簡単になった。
ちなみに、[ここ](https://openrtm.org/openrtm/node/6034)の`Linux パッケージ`で、このbashファイルをダウンロードし、上のコードの最後の行でインストールする。
#### OpenRTMのオフィシャルウェブサイトがメンテンナンスしている場合。
下のコードでインストールしてください。
```shell
cd ~/Documents/

git clone https://github.com/n-ando/xenial_package.git OpenRTM

cd OpenRTM/xenial/main/binary-amd64/

sudo dpkg -i openrtm-aist_1.1.2-0_amd64.deb
sudo dpkg -i openrtm-aist-example_1.1.2-0_amd64.deb
sudo dpkg -i openrtm-aist-dev_1.1.2-0_amd64.deb

sudo dpkg -i openrtm-aist-python_1.1.2-1_amd64.deb
sudo dpkg -i openrtm-aist-python-example_1.1.2-1_amd64.deb

rm openrtp_1.2.0-0_amd64.deb
wget https://media.githubusercontent.com/media/n-ando/xenial_package/master/xenial/main/binary-amd64/openrtp_1.2.0-0_amd64.deb  || \
wget https://media.githubusercontent.com/media/n-ando/xenial_package/master/xenial/main/binary-amd64/openrtp_1.2.0-0_amd64.deb
sudo dpkg -i openrtp_1.2.0-0_amd64.deb

# Install necessary package that not install by above deb
sudo apt-get install omniorb-nameserver omniidl omniidl-python libomniorb4-dev
```
### Pythonのまわり
```shell
sudo apt-get -y install python-visual python-numpy python-scipy python-matplotlib python-sklearn
pip install gitpython
```
### Open3D(Optional)
Open3DはIntelが2018年作ったばかりの点群処理のライブラリ。主にPythonとC++向け。Pythonで点群を処理するのは非常に便利だが、このライブラリは作ったばかりなので、実現したアルゴリズムはとても少ない。それにしても、PCLの入門ライブラリとして使うのは十分だ。
```shell
cd ~/Documents/
git clone https://github.com/IntelVCL/Open3D.git
cd Open3D

util/scripts/install-deps-ubuntu.sh
mkdir build
cd build
cmake -DPYTHON_EXECUTABLE=/usr/bin/python2.7 ../src
make -j16

cd util/scripts
./install.sh
```
### 開発環境のパスの設定
この「CookTeam\_Environment\_Setup\_Script」リポジトリにある「MakeDevEnv.sh」ファイルを実行すれば開発環境の構築ができる。しかし、注意すべき点がある：

インストール検証
--------
### Pythonの検証
適当な場所でPythonファイルを作る（例えば：`~/Workspace/SetupTest.py`）、中身は：
```python
import cv2
import numpy
import matplotlib
import sklearn
import scipy
import visual

cv2.__version__
raw_input("\nConfirm the version of OpenCV is \"3.X.0\". And press the ENTER key to confirm the visual-python installation")
visual.box()

exit()
```
作ったファイルを実行する。（セーブすることは忘れないてください。）
```shell
python ~/Workspace/Setup_Test.py
```
結果は:
![ConfirmSetup-OpenCV](fig/ConfirmSetup-OpenCV.png)
まずはエラーがあるかどうかを確認する。図の中で`FutureWarning`があるが、この警告は私たちに関係ない。エラーがないなら**[Pythonのまわり](#Pythonのまわり)**でインストールしたPythonのライブラリが正常に動ける。
そして、`Version of Opencv is 3.X.0`があれば、OpenCVのPython版が正常。
次はVPythonのインストールの確認。エンターキーを押すと下のウィンドウが現す：
![ConfirmSetup-vpython1](fig/ConfirmSetup-vpython1.png)  
右ボタンでウィンドウの中でドラッグすると、下の図になる：
![ConfirmSetup-vpython2](fig/ConfirmSetup-vpython2.png)  
先のウィンドウが現せば、vpythonも正常。これで、Pythonの検証が終わる。
### librealsense2の検証
確認は簡単で、2つのRealSenseD435を挿し（必ずUSB3.0のポート、場合により、USB3.0のHUBも必要だ）、下の命令を入力する：
```shell
~/Documents/librealsense/build/tools/realsense-viewer/realsense-viewer
```
下の図が出る：
![fig/RealSense_Viewer](fig/realsense_viewer.png)  
  
  「Add Source」をクリックして、もう一個のRealSenseD435を入れる。入れたあとは下の図になる。  
  
![fig/RealSense_Viewer_Add](fig/realsense_viewer_add.png) 
  
　そして、一つのカメラを選んで、「Stereo Module」と「RGB Camera」を「off」から「on」にする。下の図になれば、このカメラが問題なし。  
![fig/RealSense_Viewer_Res](fig/realsense_viewer_res.png)  
  
  次に、もう一個のカメラも同じようにしてください。  
  最後、2つカメラの「Stereo Module」を「on」にして、あとは「RGB Camera」も「on」にする。(4つのModuleが同時に開いたら、「Incomplete frame detected!」という警告が出るかもしれない。でも、ほっておけば大丈夫。)  
  エラーが出れば、速やかに私に連絡してください。
### librealsense-oldの検証
PA10の後ろのダンボールにあるRealSenseF200を挿し（「Hayashi」と書いたカメラが壊れた；必ずUSB3.0のポート、場合により、USB3.0のHUBも必要だ）、下の命令を入力する：
```shell
~/Documents/librealsense/build/examples/cpp-capture
```
下の図があれば、librealsenseは問題なしだ：
![ConfirmSetup-realsenseSuccess](fig/ConfirmSetup-realsenseSuccess.png)  
  
  問題があれば：
![fig/ConfirmSetup-realsenseError](fig/confirmsetup-realsenseerror.png) 

エラーが出れば、RealSenseを抜いて、下のコードを実行してください（yを選択する）。
```shell
cd ~/Documents/librealsense_old
./scripts/patch-realsense-ubuntu-xenial.sh
```
これをやっても直せない。あるいは命令を実行するときエラーが出る場合はUbuntuを再インストールしかない。古いRealSenseを使わないなら、ここまで十分だ。
### OpenRTMの検証
下の命令を入力する：
```shell
rtm-naming
```
下の図があれば、OpenRTMは問題なしだ（`and start omniNames by rtm-naming? (y/N)`が出る可能性はゼロではない。`ｙ`を選んたら、下の図のような結果が出る、`n`を選んたら、この命令が終わる。どちらを選ぶのは大丈夫で、`rtm-naming`この命令が実行できれば大丈夫。）：
![ComfirmSetup-rtmnaming](fig/ComfirmSetup-rtmnaming.png)
次はopenrtpのインストールを確認する。
下の命令を入力する：
```shell
openrtp
```
Eclipseのウィンドウがあれば、openrtpは問題なし。しかし、`eclipse`は自動的に`workspace`フォルダを作る。その名前は私達先作った`Workspace`フォルダの名前と同じ。だから、その次に出たウィンドウで、デフォルトディレクトリを`Worksapce`に変更して、eclipseが勝手に作った`workspace`を削除すれば大丈夫。

あとがき
-----------
無し

\-終わり\-
