# Update Environment of Cook-Team to Python 3.7

## Before install
Confirm you can open the tty1-6. Use Ctrl+Alt+(F1-F6) to confirm. And using Ctrl+Alt+F7 to return the GUI (For Ubuntu).

## Python 3.7 Install
For Ubuntu 16.04, there is no official distribute of python since 3.5.
So we have to use other software repopsitory to install the python3.7.
Then, Ubuntu 16.04 had python3.5 installed, we need change it to python3.7.

Use code below:
```bash
# Add new software repository, and update the local software database .
sudo add-apt-repository ppa:deadsnakes/ppa 
sudo apt-get update 

# Install.
sudo apt install python3.7

# Change the python3 from python3.5 to python 3.7.
sudo ln -sf /usr/bin/python3.7 /usr/bin/python3
sudo ln -sf /usr/bin/pip3.7 /usr/bin/pip3
sudo ln -sf /usr/bin/pip3.7 /usr/bin/pip
```

## Trouble shooting after Python 3.7 installed
### Can't open some system application or can't use some command.
After install the python 3.7, you'll encounter problems below:
1. Can not open the Terminal
2. Can not open the File.
3. Can not use `touch` command.
4. etc...

Solution of these problem is use command below:
```
sudo cp `find /usr/lib/python3*/ -name "gi"` /usr/lib/python3.7/
sudo mv /usr/lib/python3.7/_gi.*.so /usr/lib/python3.7/_gi.cpython-37m-x86_64-linux-gnu.so
sudo mv /usr/lib/python3.7/_gi_*.so /usr/lib/python3.7/_gi_cairo.cpython-37m-x86_64-linux-gnu.so
```
Many system GUI programms use python to start them self.
And some of them use the `gi` module with unknow reason.
But after new python 3.7 installed and replace the python 3.5. However, python 3.7 does not have `gi` package.
Moreover, `gi` package could not install by normal way(`pip` or `apt-get`).
So we could only "install" it by coping from old version and remaning libraries.
That is above command done.

### No pip installed
After install the python 3.7, pip may not installed by some reasons.
Solution:
1. Download [get-pip.py](#https://bootstrap.pypa.io/get-pip.py) file.
2. Using `sudo python3.7 /PATH_TO_GET-PIP.PY/get-pip.py` command to install the pip for python3.X.

## OpenRTM 1.2.0 install
2019.08.02: The Linux install shell file can be accessed, use it to install.
Or you can find this shell file in `OpenRTM install` folder.
```shell
cd ~/Downloads/
wget https://raw.githubusercontent.com/OpenRTM/OpenRTM-aist/master/scripts/pkg_install_ubuntu.sh
sudo sh ./pkg_install_ubuntu.sh -l c++ -l python -l --yes
```


## Python package installation for 3.7
* Using `pip` to install
  1. matplotlib 
  2. numpy 
  3. gitpython 
  4. omniidl 

* Install `omniorb` and `omniorbpy`

    1. Download `omniorb` and `omniorbpy`
    https://sourceforge.net/projects/omniorb/files/omniORB/omniORB-4.2.3/omniORB-4.2.3.tar.bz2/download
    https://sourceforge.net/projects/omniorb/files/omniORBpy/omniORBpy-4.2.3/omniORBpy-4.2.3.tar.bz2/download
    2. Unzip two archives to `~/Documents/` or other directories you want
    3. Compile and install
    ```shell
    cd ~/Documents/omniORB-4.2.3
    ../configure --prefix=/usr/local PYTHON=/usr/bin/python3 
    make -j4 
    sudo make install 
    ```
    ```shell
    dcd ~/Documents/omniORBpy-4.2.3
    ../configure --with-omniorb=/usr/lib --prefix=/usr/local PYTHON=/usr/bin/python3 
    make -j4 
    sudo make install 
    ```

* Install the OpenRTM python3 version (If you use dpkg package to install OpenRTM)
    * The DPKG installer do not have the python3 version, we need install it by compiltion.
    1. Download the source code:
       
       https://github.com/OpenRTM/OpenRTM-aist-Python/releases/download/v1.2.0/OpenRTM-aist-Python-1.2.0.tar.gz
    2. Unzip the archive to `~/Documents/`
    3. Use command above to install.
        ```shell
        cd ~/Documents/OpenRTM-aist-Python-1.2.0
        python3.7 setup.py build 
        python3.7 setup.py install 
        ```