SUEHIRO\_KUDOU\_LAB
===================
CookTeam\_Environment\_Setup
============================

中文（简体）
===========

目录
-----------
* [写在前面](#写在前面)
* [对系统的变动](#对系统的变动)
* [开始配置](#开始配置)
  * [确认内核版本](#确认内核版本)
  * [去除联想的大小写敏感](#去除联想的大小写敏感)
  * [librealsense](#编译安装librealsense)
  * [VTK](#编译安装vtk库)
  * [PCL](#编译安装pcl库)
  * [OpenCV](#编译安装opencv)
  * [OpenRTM](#安装openrtm库)
  * [Python相关](#安装python上常用的库)
  * [环境配置](#开发环境配置)
    - [开发文件夹设置](#克隆开发用文件夹)
    - [文件路径设置](#文件路径设置)
  * [编译RTC](#编译rtc)
* [验证安装](#验证安装)

写在前面
-----------
如果你是老手，请看[Bash文件说明](BashFile_README.md)，或者直接使用即可。
如果你是新人的话，建议还是好好读完这篇配置教程并自己手打命令，查询这些命令的作用，最好能记住一些常用命令。

配置过程中变动
-----------
1. 此bash文件执行过程中的执行记录均在`~/Documents/SetUp_Error_Msg`文件夹下。
2. 配置过程中的下载的库的源码文件以及安装程序等均在`~/Documents/`文件夹下。
3. 通过更改`/etc/profile`来实现对环境变量的添加。
4. 更改`librealsense/scripts/patch-utils.sh`28行`echo hwe`更改为`echo hwe-zesty`，并添加`realsense-camera-formats_ubuntu-xenial-hwe-zesty.patch`文件至相同目录。
5. 去掉`OpenCV/modules/core/include/opencv2/core/traits.hpp`文件中第52行的注释。
6. 创建`Workspace`文件夹，并将`base`文件夹更名为当前系统的用户名。
7. 在`/usr/lib/python2.7/`内创建`site-packages`和`dist-packages`文件夹。
8. OpenCV的Python库文件`cv2.so`复制到`site-packages`和`dist-packages`文件夹。

开始配置
-----------
### 确认内核版本
请输入`uname -r`来确认系统的内核版本。如果是`4.10.XX`的话，请放心，继续下面的步骤。如果是`4.13.XX`的话，恭喜，请先将内核切换至`4.10.XX`之后在进行下面的步骤。[内核切换步骤](ChangeKernal.md)
>**Note:**不过4.10.XX的同学还不能安心，如果没有4.13.XX内核的话（使用`dpkg -l | grep linux-image`命令来确认），执行`sudo apt-get dist-upgrade`似乎会有（需要验证）直接将内核自动升级至4.13.XX的隐患。所以，还是请先按照切换内核的步骤把内核定在`4.10.XX`之后，在进行环境配置。内核的问题主要影响RealSense，所以，如果哪天RealSense死活连不上，而且例程也报不明所以的错误的话，先查看自己的内核是否已经偷偷升级到4.13.XX.

### 去除联想的大小写敏感
Linux的Terminal中输入命令和路径的时候可以只用输入开头的几个字母后按一下Tab键系统便会自动补完剩下的部分，如果有多个候选，则会显示全部候选。默认情况下是开启大小写敏感的，不过还是没有比较方便。
```shell
sudo sh -c "echo \"set completion-ignore-case on\">>/etc/inputrc"
```
### 编译安装librealsense
料理班要用到RealSense，所以需要安装RealSense的库（一脸正经地说了句废话）。
首先，升级一下系统的软件。
```shell
sudo apt-get update
sudo apt-get update --fix-missing
sudo apt-get upgrade
```
接着，通过git命令获取librealsense的源码包。
```shell
git clone https://github.com/IntelRealsense/librealsense.git
```
由于使用的RealSense太古老，最新的librealsense已经不支持了，所以我们需要切换到以前的版本。
```shell
cd ~/Documents/librealsense #切换到librealsense目录下
git branch -a #查看所有的远程分支
git checkout -b legacy origin/legacy #切换分支
```
接下来安装librealsense必要的库文件。（`-y`选项是代表`apt-get install`询问`是否继续安装`时默认选择`y`）
```shell
sudo apt-get -y install libusb-1.0-0-dev pkg-config
sudo apt-get -y install libglfw3-dev
```
接下来便是编译安装环节了。
首先在`librealsense`内创建`build`文件夹，并进入。
```bsah
cd ~/Documents/librealsense
mkdir build
cd build
```
然后生成makefile，编译，安装一气呵成。（`make -j4`的`-j4`代表多核编译，`4`代表使用的核数，研究室的电脑基本都是四核的）
```shell
cmake .. -DBUILD_EXAMPLES=true
make -j4
sudo make install
```
由于运行RealSense摄像头需要向系统内核里面添加补丁,所以继续以下操作。（**手打命令**请先看下面的代码说明）
**WARNING:**请**[-不要-]**接入RealSense摄像头完成下列操作：
```shell
sudo cp config/99-realsense-libusb.rules /etc/udev/rules.d/
sudo udevadm control --reload-rules && udevadm trigger
sudo apt-get -y install libssl-dev
cp `dirname $0`/Realsense/*.* ~/Documents/librealsense/scripts
cd ~/Documents/librealsense
./scripts/patch-realsense-ubuntu-xenial.sh
```
>**代码说明**：
- 前三句照搬librealsense的安装指引
- 如果是**手打命令**来配置环境的话，请将代码第四行的`` `dirname $0` ``（注意这是**反引号**，日本的键盘的话按`Shift+@`，英文键盘的话就是"1"键左边的那个键）替换成`bash文件所在的文件夹`。比如：
```shell
cp ~/Documents/CookTeam_Environment_Setup_Script/Realsense/*.* ~/Documents/librealsense/scripts
```

最后，将librealsense库的目录添加到系统的环境变量`LD_LIBRARY_PATH`中。
```shell
sudo sh -c "echo \"export LD_LIBRARY_PATH=\\\${LD_LIBRARY_PATH}:/usr/local/lib/:/usr/local/lib/x86_64-linux-gnu/:\" >> /etc/profile"
source /etc/profile
sudo ldconfig
```
>**代码说明**：
- 第一句是向`/etc/profile`里添加设定环境变量的语句，Linux系统中，自己设定的环境变量会在关机后被消除，而`/etc/profile`这个文件会在系统启动的时候对所有用户执行这个文件，故起到了自动添加环境变量的效果。
- 第二句是在当前终端内执行`/etc/profile`文件
- 第三句是将当前可搜索到的库文件制作缓存，具体解释请看[ldconfig命令](http://man.linuxde.net/ldconfig "打不开？自己百度呀！")。

完了后使用`export | grep LD_LIBRARY_PATH`看一下环境变量`LD_LIBRARY_PATH`是否存在，存在的话应如下图所示：
![Env_Var_librealsense.png](fig/Env_Var_librealsense.png)
>**Note**：
- 上面的source语句仅对当前终端有效，如果是直接使用bash文件来配置的话，其他终端是看不到这个环境变量的。
- 其实没有这个环境变量也没关系，**手打命令**的话重启下电脑或者注销一下就会出现了，如果你有强迫症的话请使用`export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/lib/:/usr/local/lib/x86_64-linux-gnu/:`来添加环境变量。

### 编译安装VTK库
VTK库是一款三维可视化的一个库，具体介绍请看百度百科。对于我们料理班，之后要配置的OpenCV和PCL这两个库里面都用到了VTK库，所以如果VTK没有装好或者VTK库文件出现损坏的话，你的程序寸步难行（本人切身体会）。
虽然上面把VTK说的很重要，但是VTK的配置还是非常简单的（并不会像librealsense那样鬼畜一般复杂的配置过程）。代码如下：
```shell
sudo apt-get update
sudo apt-get update --fix-missing
sudo apt-get upgrade
sudo apt-get install libxt-dev
cd ~/Documents
wget https://www.vtk.org/files/release/8.1/VTK-8.1.0.tar.gz
wget https://www.vtk.org/files/release/8.1/VTKData-8.1.0.tar.gz
tar -xzvf VTK-8.1.0.tar.gz
tar -xzvf VTKData-8.1.0.tar.gz
cd ~/Documents/VTK-8.1.0
mkdir build
cd build
cmake ..
make -j4
sudo make install
```
看吧，多么简洁的代码！如果编译安装没有报错的话，VTK就配置完成了。**TODO:确定VTK的稳定版本**。
### 编译安装PCL库
PCL库的全称是Point Cloud Library即点云处理库，咱中国国内似乎研究的人还比较少，基本上资料还是比较初级，建议使用谷歌来搜索相关信息。配置的话，跟VTK基本一样，只不过PCL需要的依赖项远比VTK多得多，如果你看了bash文件的话，PCL那一节里面有一段很鬼畜的代码，那就是依赖项的安装。还有一点跟VTK不同的是，PCL非～～～常大，即使使用多核编译，编译所用的时间至少在45min以上，所以说料理班的环境配置的过程中，只有这个阶段是可以出去喝杯茶放松放松的（结果回来make ERROR...(滑稽)）。废话读不多说，上代码：
```shell
sudo apt-get update
sudo apt-get update --fix-missing
sudo apt-get upgrade
cd ~/Documents
git clone https://github.com/PointCloudLibrary/pcl.git pcl
sudo apt-get -y install cmake cmake-gui doxygen libphonon-dev mpi-default-dev openmpi-bin openmpi-common libflann1.8 libflann-dev libeigen3-dev libboost-all-dev libqhull* libusb-dev libgtest-dev git-core freeglut3-dev pkg-config build-essential libxmu-dev libxi-dev libusb-1.0-0-dev graphviz mono-complete qt-sdk openjdk-8-jdk openjdk-8-jre phonon-backend-gstreamer phonon-backend-vlc
cd ~/Documents/pcl
mkdir build
cd build
cmake ..
make -j4
sudo make install
```
同VTK一样，如果编译安装没有报错的话，PCL就配置完成了。
### 编译安装OpenCV
大名鼎鼎的OpenCV就不需要我来介绍了吧。。。上代码：
```shell
sudo apt-get update
sudo apt-get update --fix-missing
sudo apt-get upgrade
cd ~/Documents
git clone https://github.com/opencv/opencv.git OpenCV-master
sudo apt-get -y install build-essential libgtk2.0-dev libjpeg-dev libtiff5-dev libjasper-dev libopenexr-dev cmake python-dev python-numpy python-tk libtbb-dev libeigen3-dev yasm libfaac-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev libx264-dev libqt4-dev libqt4-opengl-dev sphinx-common texlive-latex-extra libv4l-dev libdc1394-22-dev libavcodec-dev libavformat-dev libswscale-dev default-jdk ant
cd ~/Documents/OpenCV-master
sed -i "s/\/\/#define OPENCV_TRAITS_ENABLE_DEPRECATED/#define OPENCV_TRAITS_ENABLE_DEPRECATED/" ~/Documents/OpenCV-master/modules/core/include/opencv2/core/traits.hpp
mkdir build
cd build
cmake -D VTK_DIR=/usr/local/lib/cmake/vtk-8.1\
		-D CMAKE_CXX_FLAGS=-std=c++11\
		-D CMAKE_BUILD_TYPE=RELEASE\
		-D CMAKE_INSTALL_PREFIX=/usr/local\
		-D WITH_TBB=ON\
		-D BUILD_NEW_PYTHON_SUPPORT=ON\
		-D WITH_V4L=ON\
		-D WITH_FFMPEG=OFF\
		-D BUILD_opencv_python2=ON\
		..
make -j4
sudo make install
```
>**Note**:跟libealsense一样，代码的第七行的`` `dirname $0` ``（注意**反引号**）替换成`bash文件所在的文件夹`。

编译安装完后，需要设置一下，代码如下：
```shell
sudo sh -c 'echo "/usr/local/lib">>/etc/ld.so.conf.d/opencv.conf'
sudo ldconfig
sudo mkdir /usr/lib/python2.7/site-packages/
sudo mkdir /usr/lib/python2.7/dist-packages/
sudo cp ~/Documents/OpenCV-master/build/lib/cv2.so /usr/lib/python2.7/site-packages/
sudo cp ~/Documents/OpenCV-master/build/lib/cv2.so /usr/lib/python2.7/dist-packages/
```
>**代码说明**：
- 前两句的说明参见[ldconfig命令](http://man.linuxde.net/ldconfig "打不开？自己百度呀！")。
- 剩下的四句是将OpenCV安装至Python2.7里面。

### 安装OpenRTM库
OpenRTM是日本产总研开发的一款软件中间层，说白了就是一个可以使各个程序间进行通讯的一个软件构架，通过这个软件构架，可以实现软件间（跟编程语言无关）的数据互通。
上代码：
```shell
mkdir ~/Documents/openRTM-aist_install/
cd ~/Documents/openRTM-aist_install/
wget http://svn.openrtm.org/OpenRTM-aist/trunk/OpenRTM-aist/build/pkg_install_ubuntu.sh
sudo sh ./pkg_install_ubuntu.sh -l c++ -l python -l openrtp -d --yes
```
>**代码说明**：
- 前两句：自己猜
- 第三句：从网络上下载文件至当前文件夹。
- 最后一句：安装OpenRTM，后面跟的参数是根据官方的指引以及我们的需求来进行设定（安装c++版 python版 以及openrtp 安装时默认按y，具体的参数说明请看[参数说明](https://openrtm.org/openrtm/ja/node/6345)）

某次更新后OpenRTM的安装就很简单了。只需调用他们写好的bash文件即可。
你也可以在[这里](https://openrtm.org/openrtm/node/6034)的`Linux パッケージ`里面手动下载这个文件，然后调用上面最后一句话来安装。
### 安装Python上常用的库
无需多言，上代码：
```shell
sudo apt-get -y install python-visual python-numpy python-scipy python-matplotlib
```
### 开发环境配置
本节很重要，走错一步都可能导致`找不到路径`或者`编译错误`。在本节完成前请**[-不要-]**关闭终端，因为会将配置过程中的变量消除掉，如因关闭中断导致的任何不可预测的后果，本人概不负责。
#### 克隆开发用文件夹
首先是克隆开发用的两个文件夹——`base`和`share`。这两个文件夹是由料理班的前辈们整合而成的。`share`文件夹包含了`RealSense、夹具的RTC`以及`RealSense的校准数据`以及由末广老师写的`坐标转换的库`。一般来讲`share`文件夹在开发过程中无需改动。`base`文件夹是我们开发的主体。具体还请参照**TODO:开发文档**。上代码：
```shell
cd ~
mkdir Workspace
cd Workspace
git clone http://192.168.0.9:8080/git/cook-team/share.git
git clone http://192.168.0.9:8080/git/cook-team/base.git
```
之后需要将`base`文件夹改名（base多土，自己改个好听的名字）。
```shell
UserName=`whoami`
mv base $UserName
```

>**代码说明**：
- `whoami`命令是返回当前登陆的用户名字符串，加上反引号的话`` `whoami` ``代表将`whoami`命令的结果给`UserName`这个变量。
- 这样做的话是以自己电脑的用户名来命名`base`文件夹。
- 当然你也可以自己改成自己想要的名字，直接输入就行，别加反引号，比如：

```shell
UserName=WangBigHammer
```
由于现在刚从服务器上克隆下来，所以base文件夹还在跟实验室的Git服务器处于连接状态，如果改动代码并push出去的话会直接改动前辈们辛辛苦苦构建出来的开发框架，所以我们需要先断开这个链接。
```shell
cd ~/Workspace/$UserName
sudo rm -rf .git
```
断开连接后，这个代码就可以被我们为所欲为了。
#### 文件路径设置
###### Python相关路径设置
首先在`base`文件夹内创建`set_env.py`文件。
```shell
echo "MyName = \"${UserName}\"" > ~/Workspace/$UserName/set_env.py
```
>**代码说明**：
- `>`符号是输出重定向的符号，代表将命令的输出进行重新指定，在这里是将`echo`命令的输出重新指定到`~/Workspace/$UserName/set_env.py`文件内

创建完成后看一下创建的文件是否正确。
```shell
gedit ~/Workspace/$UserName/set_env.py
```
![set_env_check](fig/set_env_check.png)
接下来在`/usr/local/lib/python2.7/dist-packages/`文件夹内创建`WorkspaceDirInfo.py`文件。并写入：（`HostName`是指你的计算机名，使用`hostname`命令查看）
```python
MyHostName = "HostName"
MyUserName = "UserName"
WorkspaceDir = "/home/"+MyUserName+"/Workspace/"
```
使用下面的命令便可全部完成，方便快捷无错误。
```shell
HostName=`hostname`
echo -e "MyHostName = \"${HostName}\"\r\nMyUserName = \"${UserName}\"\r\nWorkspaceDir = \"/home/\"+MyUserName+\"/Workspace/\"" > ~/Workspace/$UserName/WorkspaceDirInfo.py
sudo chgrp root ~/Workspace/$UserName/WorkspaceDirInfo.py
sudo chown root ~/Workspace/$UserName/WorkspaceDirInfo.py
sudo chmod 644 ~/Workspace/$UserName/WorkspaceDirInfo.py
sudo mv ~/Workspace/$UserName/WorkspaceDirInfo.py /usr/local/lib/python2.7/dist-packages/WorkspaceDirInfo.py
```
>**代码说明**：
- 第一句：刚解释过了
- 第二句：`-e`选项可以对转义字符进行输出。具体请自己百度。
- 接下来的三句：设定文件的用户组、所有权、权限。
- 最后一句：将建好的文件放到它该在的位置。

检视文件。
```shell
sudo gedit /usr/local/lib/python2.7/dist-packages/WorkspaceDirInfo.py
```
![WorkspaceDirInfo_check](fig/workspacedirinfo_check.png)
###### RealSense摄像头校准数据路径设置
在`/usr/local/include/openRTM/`文件夹内创建`pathInfo.h`文件。并写入：（如果是手动创建的话，记得将`${UserName}`换成自己的名字）
```c++
#include <iostream>
static std::string calib_path = "/home/${UserName}/Workspace/share/data/RealSense/";
```
当然跟上面一样，使用下面的命令便可全部完成。
```shell
sudo mkdir /usr/local/include/openRTM
echo -e "#include <iostream>\r\nstatic std::string calib_path = \"/home/${UserName}/Workspace/share/data/RealSense/\";" > ~/Workspace/$UserName/pathInfo.h
sudo chgrp root ~/Workspace/$UserName/pathInfo.h
sudo chown root ~/Workspace/$UserName/pathInfo.h
sudo chmod 644 ~/Workspace/$UserName/pathInfo.h
sudo mv ~/Workspace/$UserName/pathInfo.h /usr/local/include/openRTM/pathInfo.h
```
检视文件。
```shell
sudo gedit /usr/local/include/openRTM/pathInfo.h
```
![pathInfo_check](fig/pathinfo_check.png)
至此，路径设置全部完成。
### 编译RTC
共编译RTC\_Realsense、RTC\_ImageProcess、RTC\_PointCloudProcess这三个RTC。
* RTC\_Realsense包含了RealSense摄像头的数据获取、类型转换等代码。
* RTC\_ImageProcess是实现二维图像的处理的代码的RTC。
* RTC\_PointCloudProcess是实现三维点云的处理的代码的RTC。

上代码：
```shell
cd ~/Workspace/${UserName}/rtc/RTC_ImageProcess
rm CMakeCache.txt
cmake .
make -j4

cd ~/Workspace/${UserName}/rtc/RTC_PointCloudProcess
rm CMakeCache.txt
cmake .
make -j4

cd ~/Workspace/share/rtc/RTC_Realsense
rm CMakeCache.txt
cmake .
make -j4
```
如果没出错的话，恭喜你，料理班的环境至此已经全部配置完成。（撒花～～～～～）

验证安装
--------
### Python相关
随意找个地方创建个python文件（比如：`~/Workspace/SetupTest.py`），并写入：
```python
import cv2
import numpy
import matplotlib
import scipy
import visual

cv2.__version__
raw_input("\nConfirm the version of OpenCV is \"3.X.0\". And press the ENTER key to confirm the visual-python installation")
visual.box()

exit()
```
然后执行刚才创建好的文件（记得要保存）
```shell
python ~/Workspace/Setup_Test.py
```
结果如下图:
![ConfirmSetup-OpenCV](fig/ConfirmSetup-OpenCV.png)
首先确认有没有报错，图里面虽然有一个`FutureWarning`，但这个无关痛痒。没有报错的话，之前在**[安装Python上常用的库](#安装Python上常用的库)**安装的几个Python库均已正常。
之后确认`Version of Opencv is 3.X.0`这句话是否出现，出现的话OpenCV的Python版就成功安装了。
接下来按回车来确认VPython的安装。
结果会跳出来一个带黑边名为`VPython`的窗口，如下图所示：
![ConfirmSetup-vpython1](fig/ConfirmSetup-vpython1.png)
emmmmmmmm，我不是要显示一个方块么，咋成正方形了。别怕，鼠标右键在窗口内拖动一下就变成下图的样子了：
![ConfirmSetup-vpython2](fig/ConfirmSetup-vpython2.png)
Python相关的安装到此确认结束。
### librealsense相关
确认很简单，插上RealSense（必须是USB3.0视情况还需要通过USB3.0HUB来接入），运行以下代码：
```shell
~/Documents/librealsense/build/examples/cpp-capture
```
出现下图的话，librealsense就没问题了：
![ConfirmSetup-realsenseSuccess](fig/ConfirmSetup-realsenseSuccess.png)
如果出现问题的话，请参照：**TODO:RealSense_Error_Solutions.md**
### OpenRTM相关
运行下面的代码：
```shell
rtm-naming
```
出现下图的话，OpenRTM就没问题了（也有可能出现`and start omniNames by rtm-naming? (y/N)`，选`y`的话就会弹出如下图的结果，选`n`的话直接退出，不过选哪个都无所谓，只要`rtm-naming`这个命令可以正常运行就行）：
![ComfirmSetup-rtmnaming](fig/ComfirmSetup-rtmnaming.png)
接下来确认RTC管理器软件的安装，这个软件基于Eclipse。
运行下面的代码：
```shell
openrtp
```
正常出现`eclipse`窗口的话，就正常了。不过eclipse会自动创建一个叫`workspace`的文件夹，这个名字刚好和我们的`Workspace`冲突，所以弹出窗口的时候把默认的文件夹改成我们的`Workspace`，再将自动创建的`workspace`文件夹删除即可。

结语
-----------
还没想好。
