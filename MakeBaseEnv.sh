#!/bin/bash

OutMsg()
{
	echo -ne "\e[1;36m" #\e and \033 is Esc key.
	echo -ne $1
	echo -ne "\e[0m"
	read -p "" nulll
}
Directory_ErrMSG=~/Documents/SetUp_Error_Msg/
ShellFileDir=`cd \`dirname $0\`; pwd`

set -x #Display commands that are executed now.
mkdir $Directory_ErrMSG
set +x



#1. 入力補完の大文字小文字の区別を取り去る
	sudo apt-get -y install cmake | tee -a $Directory_ErrMSG/CMAKE_INSTALL_Out.txt
	OutMsg "1. Start to make input completion without case sense.\nPress any key to continue."
	set -x
	sudo sh -c "echo \"set completion-ignore-case on\">>/etc/inputrc"
	set +x
	

############################################################################
#1.	librealsense2 のインストール
############################################################################
	OutMsg "#########################################\n\
			#1 librealsense installation.\n\
			#########################################\n\
			Press any key to continue."
	# アップデート
	sudo apt-get update | tee -a $Directory_ErrMSG/Librealsense_update_Out.txt
	sudo apt-get update --fix-missing | tee -a $Directory_ErrMSG/Librealsense_update_Out.txt
	sudo apt-get upgrade | tee -a $Directory_ErrMSG/Librealsense_update_Out.txt
	OutMsg "1.1 Update over. Press any key to continue librealsense install."

	# librealsense ソースコードのダウンロード
	sudo apt-get -y install git | tee -a $Directory_ErrMSG/Librealsense_git_Out.txt
	cd ~/Documents
	git clone https://github.com/IntelRealsense/librealsense.git | tee -a $Directory_ErrMSG/Librealsense_git_Out.txt
	cd ~/Documents/librealsense
	
	sudo apt-get -y install libusb-1.0-0-dev pkg-config | tee -a $Directory_ErrMSG/Librealsense_4.2.4_Out.txt
	sudo apt-get -y install libglfw3-dev libgtk-3-dev | tee -a $Directory_ErrMSG/Librealsense_4.2.4_Out.txt

	OutMsg "1.2 Start to build librealsense. Press any key to continue."
	cd ~/Documents/librealsense
	mkdir build
	cd build

	cmake .. -DBUILD_EXAMPLES=true  | tee -a $Directory_ErrMSG/Librealsense_CMAKE_Out.txt

	make -j4 | tee -a $Directory_ErrMSG/Librealsense_make_Out.txt
	sudo make install | tee -a $Directory_ErrMSG/Librealsense_make_Out.txt
	
	OutMsg "1.3 Start to setup Video4Linux backend. Please confirm the RealSense is NOT plug into computer. Press any key to continue."
	OutMsg "Realsense が PC に刺さっていないことを必ず確認する.\n请 不要 将Realsense插到电脑上. \nPlease confirm the RealSense is NOT plug into computer. \nPress any key to continue."
	OutMsg "本当に確認した？.\n请再次确认 Realsense 未 插到电脑上. \nAre you sure? \nPress any key to SETUP Video4Linux backend."
	set -x
	sudo cp config/99-realsense-libusb.rules /etc/udev/rules.d/
	sleep 5
	sudo udevadm control --reload-rules && udevadm trigger | tee -a $Directory_ErrMSG/Librealsense_others_Out.txt
	sleep 5
	sudo apt-get -y install libssl-dev | tee -a $Directory_ErrMSG/Librealsense_others_Out.txt
	sleep 5
	set +x
	
	cd ~/Documents/librealsense
	./scripts/patch-realsense-ubuntu-xenial.sh | tee -a $Directory_ErrMSG/Librealsense_others_Out.txt


############################################################################
#2.	librealsense-old のインストール
############################################################################
	OutMsg "#########################################\n\
			#2	librealsense-old installation.\n\
			#########################################\n\
			ress any key to continue."

	#2.1. librealsense ソースコードのダウンロード
	cd ~/Documents
	git clone https://github.com/IntelRealsense/librealsense.git librealsense_old | tee -a $Directory_ErrMSG/Librealsense_git_Out.txt
	
	OutMsg "2.1 Start to build librealsense. Press any key to continue."
	cd ~/Documents/librealsense_old
	mkdir build
	cd build

	cmake .. -DBUILD_EXAMPLES=true  | tee -a $Directory_ErrMSG/Librealsense_CMAKE_Out.txt

	make -j4 | tee -a $Directory_ErrMSG/Librealsense_make_Out.txt
	sudo make install | tee -a $Directory_ErrMSG/Librealsense_make_Out.txt

	cd ~/Documents/librealsense_old
	# cp ${ShellFileDir}/Realsense/*.* ~/Documents/librealsense/scripts  # Change file for 4.10 kernal support.
	# ./scripts/patch-realsense-ubuntu-xenial.sh | tee -a $Directory_ErrMSG/Librealsense_others_Out.txt


	# Add environment variable for all users.
	sudo sh -c "echo \"export LD_LIBRARY_PATH=\\\${LD_LIBRARY_PATH}:/usr/local/lib/:/usr/local/lib/x86_64-linux-gnu/:\" >> /etc/profile"
	source /etc/profile
	export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/lib/:/usr/local/lib/x86_64-linux-gnu/:  # Only for installation test.

	sudo ldconfig


############################################################################
#3. VTK-master
############################################################################
	echo -e"###############\n\
			#3. VTK-master.\n\
			###############\n"
	OutMsg "3.1 Start to get VTK from GIT. Press any key to continue."
	sudo apt-get update | tee -a $Directory_ErrMSG/VTK_apt_update_Out.txt
	sudo apt-get update --fix-missing | tee -a $Directory_ErrMSG/VTK_apt_update_Out.txt
	sudo apt-get upgrade | tee -a $Directory_ErrMSG/VTK_apt_update_Out.txt
	sudo apt-get install libxt-dev
	cd ~/Documents
	#git clone https://gitlab.kitware.com/vtk/vtk.git vtk-master | tee -a $Directory_ErrMSG/VTK_git_Out.txt
	wget https://www.vtk.org/files/release/8.1/VTK-8.1.0.tar.gz
	wget https://www.vtk.org/files/release/8.1/VTKData-8.1.0.tar.gz
	tar -xzvf VTK-8.1.0.tar.gz
	tar -xzvf VTKData-8.1.0.tar.gz
	
	OutMsg "3.2 Start to build VTK. \nPress any key to build VTK."
	cd ~/Documents/VTK-8.1.0
	mkdir build
	cd build

	cmake .. | tee -a $Directory_ErrMSG/VTK_CMAKE_Out.txt

	make -j4 | tee -a $Directory_ErrMSG/VTK_make_Out.txt
	sudo make install | tee -a $Directory_ErrMSG/VTK_make_Out.txt


############################################################################
#4. PCL-master
############################################################################
	OutMsg "#########################################\n\
			# 4. PCL-master\n\
			#########################################\n\
			Press any key to continue."
	OutMsg "4.1 Start to get PointCloudLibrary from GITHUB. Press any key to continue."
	sudo apt-get update | tee -a $Directory_ErrMSG/PCL_apt_update_Out.txt
	sudo apt-get update --fix-missing | tee -a $Directory_ErrMSG/PCL_apt_update_Out.txt
	sudo apt-get upgrade | tee -a $Directory_ErrMSG/PCL_apt_update_Out.txt
	cd ~/Documents
	git clone https://github.com/PointCloudLibrary/pcl.git pcl | tee -a $Directory_ErrMSG/PCL_git_Out.txt

	OutMsg "4.2 Start to get dependence for PCL. Press any key to continue."
	sudo apt-get -y install cmake cmake-gui doxygen mpi-default-dev openmpi-bin openmpi-common libflann1.8 libflann-dev libeigen3-dev libboost-all-dev libqhull* libusb-dev libgtest-dev git-core freeglut3-dev pkg-config build-essential libxmu-dev libxi-dev libusb-1.0-0-dev graphviz mono-complete libphonon-dev qt-sdk openjdk-8-jdk openjdk-8-jre phonon-backend-gstreamer phonon-backend-vlc | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt

	OutMsg "4.3 Start to build PCL.\n It may take one~two hour. \nPress any key to build PCL."
	cd ~/Documents/pcl
	mkdir build
	cd build

	cmake .. | tee -a $Directory_ErrMSG/PCL_CMAKE_Out.txt

	make -j4 | tee -a $Directory_ErrMSG/PCL_make_Out.txt
	sudo make install | tee -a $Directory_ErrMSG/PCL_make_Out.txt


############################################################################
#5 OpenCV-master
############################################################################
	OutMsg "###################\n\
			# 3. OpenCV-master.\n\
			###################\n\
			Press any key to continue."
	sudo apt-get update | tee -a $Directory_ErrMSG/OPENCV_apt_update_Out.txt
	sudo apt-get update --fix-missing | tee -a $Directory_ErrMSG/OPENCV_apt_update_Out.txt
	sudo apt-get upgrade | tee -a $Directory_ErrMSG/OPENCV_apt_update_Out.txt
	OutMsg "3.1 Start to get OpenCV from GITHUB. Press any key to continue."

	Name_of_OpenCV_Folder="OpenCV-master"
	
	cd ~/Documents
	git clone https://github.com/opencv/opencv.git $Name_of_OpenCV_Folder | tee -a $Directory_ErrMSG/OPENCV_GitHUB_Out.txt

	sudo apt-get -y install build-essential libgtk2.0-dev libjpeg-dev libtiff5-dev libjasper-dev libopenexr-dev cmake python-dev python-numpy python-tk libtbb-dev libeigen3-dev yasm libfaac-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev libx264-dev libqt4-dev libqt4-opengl-dev sphinx-common texlive-latex-extra libv4l-dev libdc1394-22-dev libavcodec-dev libavformat-dev libswscale-dev default-jdk ant | tee -a $Directory_ErrMSG/OPENCV_apt_get_Out.txt

	OutMsg "3.2 Start to build OpenCV. Press any key to continue."
	cd ~/Documents/$Name_of_OpenCV_Folder
	sed -i "s/\/\/#define OPENCV_TRAITS_ENABLE_DEPRECATED/#define OPENCV_TRAITS_ENABLE_DEPRECATED/" ~/Documents/$Name_of_OpenCV_Folder/modules/core/include/opencv2/core/traits.hpp
	mkdir build
	cd build

	cmake -D VTK_DIR=/usr/local/lib/cmake/vtk-8.1\
			-D CMAKE_CXX_FLAGS=-std=c++11\
			-D CMAKE_BUILD_TYPE=RELEASE\
			-D CMAKE_INSTALL_PREFIX=/usr/local\
			-D WITH_TBB=ON\
			-D BUILD_NEW_PYTHON_SUPPORT=ON\
			-D WITH_V4L=ON\
			-D WITH_FFMPEG=OFF\
			-D BUILD_opencv_python2=ON\
			.. \
			| tee -a $Directory_ErrMSG/OPENCV_CMAKE_Out.txt

	make -j4 | tee -a $Directory_ErrMSG/OPENCV_make_Out.txt
	sudo make install | tee -a $Directory_ErrMSG/OPENCV_make_Out.txt
	
	set -x
	sudo sh -c 'echo "/usr/local/lib">>/etc/ld.so.conf.d/opencv.conf'
	sudo ldconfig
	
	sudo mkdir /usr/lib/python2.7/site-packages/
	sudo mkdir /usr/lib/python2.7/dist-packages/
	sudo cp ~/Documents/$Name_of_OpenCV_Folder/build/lib/cv2.so /usr/lib/python2.7/site-packages/
	sudo cp ~/Documents/$Name_of_OpenCV_Folder/build/lib/cv2.so /usr/lib/python2.7/dist-packages/
	set +x
	
	
############################################################################
#6. Make openRTM-aist's environment.
############################################################################
	OutMsg "#####################################\n\
			# 6. Make openRTM-aist's environment.\n\
			#####################################\n\
			Press any key to continue."
#	OutMsg "6.1 Start to install the openRTM-aist. Press any key to continue."
#
#	mkdir ~/Documents/openRTM-aist_install/
#	cd ~/Documents/openRTM-aist_install/
#	wget http://svn.openrtm.org/OpenRTM-aist/trunk/OpenRTM-aist/build/pkg_install_ubuntu.sh | tee -a $Directory_ErrMSG/OPENRTM_rtm_get_Out.txt
#	
#	sudo sh ./pkg_install_ubuntu.sh -l c++ -l python -l openrtp -d --yes

######################################
# 6. Temporary way to install OpenRTM.
# Now OopenRTM's website can not 
# access, so we have to install with 
# another way.
######################################
	git clone https://github.com/n-ando/xenial_package.git OpenRTM

	cd OpenRTM/xenial/main/binary-amd64/

	sudo dpkg -i openrtm-aist_1.1.2-0_amd64.deb
	sudo dpkg -i openrtm-aist-example_1.1.2-0_amd64.deb
	sudo dpkg -i openrtm-aist-dev_1.1.2-0_amd64.deb

	sudo dpkg -i openrtm-aist-python_1.1.2-1_amd64.deb
	sudo dpkg -i openrtm-aist-python-example_1.1.2-1_amd64.deb
	
	rm openrtp_1.2.0-0_amd64.deb
	wget https://media.githubusercontent.com/media/n-ando/xenial_package/master/xenial/main/binary-amd64/openrtp_1.2.0-0_amd64.deb  || \
	wget https://media.githubusercontent.com/media/n-ando/xenial_package/master/xenial/main/binary-amd64/openrtp_1.2.0-0_amd64.deb
	sudo dpkg -i openrtp_1.2.0-0_amd64.deb
	
	# Install necessary package that not install by above deb
	sudo apt-get install omniorb-nameserver omniidl omniidl-python libomniorb4-dev


############################################################################
#7. visual python, numpy, matplotlib, scipy
############################################################################
	OutMsg "#########################################\n#7. visual python, numpy, matplotlib, scipy\n#########################################\nPress any key to continue."
	sudo apt-get -y install python-visual python-numpy python-scipy python-matplotlib sklearn | tee -a $Directory_ErrMSG/PYTHON_lib_install_Out.txt


############################################################################
#8. Confirm installation
############################################################################
	echo -e "########################\n\
			# 6. Confirm installation\n\
			#########################"
	OutMsg "6.1 Start to confirm python modules installation."
	echo -e "
	import cv2
	import numpy
	import matplotlib
	import sklearn
	import scipy
	import visual

	print \"\\nYour OpenCV\'s version is \" + cv2.__version__
	raw_input(\"Confirm the version of OpenCV is \\\"3.X.0\\\". And press the ENTER key to confirm the visual-python installation\")
	visual.box()

	exit()
	" > ~/Workspace/$UserName/Setup_Test.py

	python ~/Workspace/$UserName/Setup_Test.py

	OutMsg "8.2 Start to confirm RealSense-old installation. Please plug the RealSenseF200 and Press any key to continue."
	~/Documents/librealsense_old/build/examples/cpp-capture

	OutMsg "8.3 Start to confirm RealSense-NEW installation. Please plug the RealSenseD435 and Press any key to continue."
	~/Documents/librealsense/build/tools/realsense-viewer/realsense-viewer

	OutMsg "8.4 Start to confirm naming server installation. Press any key to continue."
	rtm-naming

	OutMsg "8.5 Start to confirm OpenRTP installation. Press any key to continue."
	openrtp
