#!/bin/bash

# PA10Left
timestamp=`date +%Y_%m_%d__%H_%M_%S`
publickey_name=PA10Left_`users`_$timestamp
ssh-keygen -t rsa -f ~/.ssh/$publickey_name

ssh-copy-id -i ~/.ssh/$publickey_name.pub pa10@192.168.3.28


# PA10Right
timestamp=`date +%Y_%m_%d__%H_%M_%S`
publickey_name=PA10Left_`users`_$timestamp
ssh-keygen -t rsa -f ~/.ssh/$publickey_name

ssh-copy-id -i ~/.ssh/$publickey_name.pub pa10@192.168.3.44
