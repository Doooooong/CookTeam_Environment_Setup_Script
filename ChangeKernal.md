Change Ubuntu16.04 Kernal version
===========
LANGUAGE
-----------
- [中文（简体）](#中文)
- [日本語](#日本語)
- English????

中文
-----------
1. 在终端输入
``` bash
	grep -n -o "menuentry [\']Ubuntu.*-generic[\']" /boot/grub/grub.cfg
```
结果如图所示：  
![findkernal](fig/findkernal.png)  
记录想要设定的内核版本的行号（深绿色数字）。  

3. 然后在`/boot/grub/grub.cfg`文件内找到刚才记录的所在行，并将menuentry那一整段代码（如下面那段代码所示）复制到`/etc/grub.d/40_custom`文件的末尾内，并保存。
比如：
``` grub
menuentry 'Ubuntu, with Linux 4.10.0-28-generic' --class ubuntu --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-4.10.0-28-generic-advanced-08cd3c7c-55d5-4c90-bb44-462c929fbd9f' { 
recordfail 
load_video 
gfxmode $linux_gfx_mode 
insmod gzio 
if [ x$grub_platform = xxen ]; then insmod xzio; insmod lzopio; fi 
insmod part_gpt 
insmod ext2 
set root='hd0,gpt7' 
if [ x$feature_platform_search_hint = xy ]; then 
  search --no-floppy --fs-uuid --set=root --hint-bios=hd0,gpt7 --hint-efi=hd0,gpt7 --hint-baremetal=ahci0,gpt7  08cd3c7c-55d5-4c90-bb44-462c929fbd9f 
else 
  search --no-floppy --fs-uuid --set=root 08cd3c7c-55d5-4c90-bb44-462c929fbd9f 
fi 
echo	'Loading Linux 4.10.0-28-generic ...' 
linux	/boot/vmlinuz-4.10.0-28-generic.efi.signed root=UUID=08cd3c7c-55d5-4c90-bb44-462c929fbd9f ro  quiet splash $vt_handoff 
echo	'Loading initial ramdisk ...' 
initrd	/boot/initrd.img-4.10.0-28-generic 
} 
```

4. 修改`/etc/default/grub`文件的`GRUB_DEFAULT=【对应默认启动菜单项序号（从0开始数）】`。嘛，不过按照上面的步骤做下来的默认情况应该是4（也就是第五项）。
5. 终端内输入
```bash
	sudo update-grub
```

6. 重启
7. 启动过程中的界面上应该比改之前多了一个`Ubuntu，Linux 4.10.0-28-generic`的选项，而且默认是选在这个选项上的。

日本語
----------
1. ターミナルで下の命令を入力する
``` bash
	grep -n -o "menuentry [\']Ubuntu.*-generic[\']" /boot/grub/grub.cfg
```
結果は：  
![findkernal](fig/findkernal.png)  
設定したいカーネルバージョンの行番号（緑色の数字）。  

3. 次に、`/boot/grub/grub.cfg`ファイルで先記録した行番号を探して、下のコードのように全部`/etc/grub.d/40_custom`にコピーする。
``` grub
menuentry 'Ubuntu, with Linux 4.10.0-28-generic' --class ubuntu --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-4.10.0-28-generic-advanced-08cd3c7c-55d5-4c90-bb44-462c929fbd9f' { 
recordfail 
load_video 
gfxmode $linux_gfx_mode 
insmod gzio 
if [ x$grub_platform = xxen ]; then insmod xzio; insmod lzopio; fi 
insmod part_gpt 
insmod ext2 
set root='hd0,gpt7' 
if [ x$feature_platform_search_hint = xy ]; then 
  search --no-floppy --fs-uuid --set=root --hint-bios=hd0,gpt7 --hint-efi=hd0,gpt7 --hint-baremetal=ahci0,gpt7  08cd3c7c-55d5-4c90-bb44-462c929fbd9f 
else 
  search --no-floppy --fs-uuid --set=root 08cd3c7c-55d5-4c90-bb44-462c929fbd9f 
fi 
echo	'Loading Linux 4.10.0-28-generic ...' 
linux	/boot/vmlinuz-4.10.0-28-generic.efi.signed root=UUID=08cd3c7c-55d5-4c90-bb44-462c929fbd9f ro  quiet splash $vt_handoff 
echo	'Loading initial ramdisk ...' 
initrd	/boot/initrd.img-4.10.0-28-generic 
} 
```
4. `/etc/default/grub`の中の`GRUB_DEFAULT=デフォルト起動オプションの番号（ゼロから数える）】`。上の手順なら、ここで4になる（つまり五番目だ）。
5. ターミナルで下の命令を入力する
```bash
	sudo update-grub
```
6. 再起動
7. 起動オプションの画面は`Ubuntu，Linux 4.10.0-28-generic`というオプションが表すはずだ、それに最初からこのオプションが選択された。
